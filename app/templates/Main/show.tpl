{block content}
	<section class="homepage-text">
		<h1>{define title}{$title}{/define}</h1>
		{!$text}
	</section>

	{if $clanky}
		<section class="articles">
			{foreach $clanky as $clanek}
				<article>
					<h1>{$clanek['name']}</h1>

					<div class="image-wrap">
						<img src="{$clanek['image']}" alt="Article image">
					</div>

					<p class="text">{$clanek['text']}</p>

					<footer>
						<a href="{link Articles::show::$clanek['seo_url']}" class="green-button" data-j-link>Více</a>
						<span class="comments">0</span>
						<span class="date">{= (new Date()).setTime($clanek['date'])}</span>
					</footer>
				</article>
			{/foreach}
		</section>
	{/if}

	{*<section class="articles">*}

		{*<h1>{define title}Blog{/define}</h1>*}

		{*<article>*}
			{*<h1>A zase ten JavaScript</h1>*}

			{*<div class="image-wrap">*}
				{*<img src="images/node-js-article-img.png" alt="User default icon">*}
			{*</div>*}

			{*<p class="text">*}
				{*Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt vel tortor in viverra.*}
				{*Etiam purus nulla, mollis porta ligula vel, dictum ultricies lectus. Curabitur ut risus odio.*}
				{*Donec in enim blandit, varius nunc eget, suscipit enim. Interdum et malesuada fames ac ante ipsum*}
				{*primis in faucibus. Donec euismod augue vel metus viverra convallis. Praesent rutrum, arcu sit amet*}
				{*placerat euismod, lorem est lobortis sem, eget tempus diam eros eget ex. Curabitur sodales quam sed*}
				{*pretium malesuada. Integer nisi mi, fermentum vitae augue cursus, consectetur luctus justo. Praesent*}
				{*eget libero viverra, aliquam magna id, rhoncus velit. Sed sed lacus pretium, volutpat nulla a,*}
				{*elementum lorem. Aliquam et nisl sed purus ultricies bibendum id eu dui.*}
			{*</p>*}

			{*<footer>*}
				{*<a href="/article/1-a-zase-ten-javascript" class="green-button" data-j-link>Více</a>*}
				{*<span class="comments">5</span>*}
				{*<span class="date">13.1.2016</span>*}
			{*</footer>*}
		{*</article>*}


		{*<article>*}
			{*<h1>A zase ten JavaScript</h1>*}

			{*<div class="image-wrap">*}
				{*<img src="images/node-js-article-img.png" alt="User default icon">*}
			{*</div>*}

			{*<p class="text">*}
				{*Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt vel tortor in viverra.*}
				{*Etiam purus nulla, mollis porta ligula vel, dictum ultricies lectus. Curabitur ut risus odio.*}
				{*Donec in enim blandit, varius nunc eget, suscipit enim. Interdum et malesuada fames ac ante ipsum*}
				{*primis in faucibus. Donec euismod augue vel metus viverra convallis. Praesent rutrum, arcu sit amet*}
				{*placerat euismod, lorem est lobortis sem, eget tempus diam eros eget ex. Curabitur sodales quam sed*}
				{*pretium malesuada. Integer nisi mi, fermentum vitae augue cursus, consectetur luctus justo. Praesent*}
				{*eget libero viverra, aliquam magna id, rhoncus velit. Sed sed lacus pretium, volutpat nulla a,*}
				{*elementum lorem. Aliquam et nisl sed purus ultricies bibendum id eu dui.*}
			{*</p>*}

			{*<footer>*}
				{*<a href="/article/1-a-zase-ten-javascript" class="green-button" data-j-link>Více</a>*}
				{*<span class="comments">5</span>*}
				{*<span class="date">13.1.2016</span>*}
			{*</footer>*}
		{*</article>*}


		{*<article>*}
			{*<h1>A zase ten JavaScript</h1>*}

			{*<div class="image-wrap">*}
				{*<img src="images/node-js-article-img.png" alt="User default icon">*}
			{*</div>*}

			{*<p class="text">*}
				{*Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tincidunt vel tortor in viverra.*}
				{*Etiam purus nulla, mollis porta ligula vel, dictum ultricies lectus. Curabitur ut risus odio.*}
				{*Donec in enim blandit, varius nunc eget, suscipit enim. Interdum et malesuada fames ac ante ipsum*}
				{*primis in faucibus. Donec euismod augue vel metus viverra convallis. Praesent rutrum, arcu sit amet*}
				{*placerat euismod, lorem est lobortis sem, eget tempus diam eros eget ex. Curabitur sodales quam sed*}
				{*pretium malesuada. Integer nisi mi, fermentum vitae augue cursus, consectetur luctus justo. Praesent*}
				{*eget libero viverra, aliquam magna id, rhoncus velit. Sed sed lacus pretium, volutpat nulla a,*}
				{*elementum lorem. Aliquam et nisl sed purus ultricies bibendum id eu dui.*}
			{*</p>*}

			{*<footer>*}
				{*<a href="/article/1-a-zase-ten-javascript" class="green-button" data-j-link>Více</a>*}
				{*<span class="comments">5</span>*}
				{*<span class="date">13.1.2016</span>*}
			{*</footer>*}
		{*</article>*}
	{*</section>*}
{/block}