{block content}
	<section class="page-text">
		<h1>{define title}{$title}{/define}</h1>
		{!$text}
	</section>

	{if $clanky}
		<section class="articles">
			{foreach $clanky as $clanek}
				<article>
					<h1>{$clanek['name']}</h1>

					<div class="image-wrap">
						<img src="{$clanek['image']}" alt="Article image">
					</div>

					<p class="text">{$clanek['text']}</p>

					<footer>
						<a href="{link Articles::show::$clanek['seo_url']}" class="green-button" data-j-link>Více</a>
						<span class="comments">0</span>
						{*<span class="date">{=(new Jumbo.Utils.Date($clanek['date'] * 1000)).getFormat("d.m.Y")}</span>*}
						<span class="date">{=$test = new Date();$test.setTime($clanek['date'] * 1000);console.log($test);$test.toLocaleDateString()}</span>
					</footer>
				</article>
			{/foreach}
		</section>
	{/if}
{/block}