{if $formErrorMessages}
	<p>
		{foreach $formErrorMessages as $message}
			<span class="form-error">{$message}</span><br>
		{/foreach}
	</p>
{/if}