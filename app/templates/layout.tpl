<!DOCTYPE html>
<html {if $lang}lang="{$lang}"{/if}>
	<head>
		<meta charset="utf-8">
		<meta name='viewport' content='width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;'>


		<title>{defined title}{use title} | {/defined}RJDev.net - Node.js, PHP, .NET, Java, web (HTML, CSS/LESS, JS) developing</title>


		<link rel="stylesheet" href="/rjlib/default-style.css">

		<link rel="stylesheet" href="/css/style.css">
		<link rel='stylesheet' media='(max-width: 6in)' href="/css/mobile.css">


		<script src="/rjlib/base/core.js"></script>
		<script src="/rjlib/base/xhr.js"></script>
		<script src="/rjlib/base/app.js"></script>
		<script src="/rjlib/base/dom.js"></script>
		<script src="/rjlib/base/effects.js"></script>
		<script src="/rjlib/base/dynamics.js"></script>
		<script src="/rjlib/base/_class.js"></script>
		<script src="/rjlib/base/Dialog.js"></script>
		<script src="/rjlib/base/windows.js"></script>
		<script src="/rjlib/base/prototype-edit.js"></script>

		<script src="/js/script.js"></script>


		{include head}


		<script>
			{if $errorMessages}
				{foreach $errorMessages as $message}
					alert("{$message}");
				{/foreach}
			{/if}
		</script>
	</head>
	<body>
		<div class="top">
			<div class="center-wrap">
				<nav id="navigation">
					<ul>
						<li><a href="/" data-j-link>Home</a></li>
						<li><a href="/aboutme" data-j-link>O mně</a></li>
						<li><a href="/projects" data-j-link>Projekty</a></li>
						<li><a href="/blog" data-j-link>Blog</a></li>
						<li class="back-to-top"><a data-j-onclick="backToTop()"k>Back to top</a></li>
					</ul>

					<div class="mobile-menu" data-j-onclick="showMenu()"></div>
				</nav>

				<header>
					<h1 onclick="location.href='/'">RJDev.NET</h1>
					<h2>Než-li býti hloupou ovcí bez základních znalostí,<br> to raději znovu vynaleznu kolo.</h2>
					<a href="/explanation" class="green-button" data-j-link>To chci vysvětlit</a>
				</header>
			</div>
		</div>

		<div class="under-top-line">
			<div class="blue-bg-fix"></div>
			<div class="center-wrap">
				<div class="state">
					<span>Celkový počet článků: 0</span>
					<span>Celkový počet uživatelů: 0</span>
					<span>Celkový počet komentářů: 0</span>
				</div>

				<div class="icons">
					<h2>Node.JS</h2>
					<h2>HTML(5)</h2>
					<h2>CSS(3)</h2>
					<h2>JavaScript (RJLib, jQuery, AngularJS)</h2>
					<h2>.NET</h2>
					<h2>Java</h2>
				</div>
			</div>
		</div>

		<div class="center-wrap">
			<main data-j-content>
				{include content}
			</main>

			<aside class="articles">
				<h2>Poslední články</h2>
				<div class="block">
					<div class="item last-article">
						<a href="/article/1-a-zase-ten-javascript" data-j-link>
							<div class="image-wrap">
								<img src="/images/user-default-ico.png" alt="Article image">
							</div>

							<span class="name">A zase ten JavaScript</span>
							<span class="date">14.1.2016</span>
						</a>
					</div>

					<div class="item last-article">
						<a href="/article/1-a-zase-ten-javascript" data-j-link>
							<div class="image-wrap">
								<img src="/images/user-default-ico.png" alt="Article image">
							</div>

							<span class="name">A zase ten JavaScript</span>
							<span class="date">14.1.2016</span>
						</a>
					</div>

					<div class="item last-article">
						<a href="/article/1-a-zase-ten-javascript" data-j-link>
							<div class="image-wrap">
								<img src="/images/user-default-ico.png" alt="Article image">
							</div>

							<span class="name">A zase ten JavaScript</span>
							<span class="date">14.1.2016</span>
						</a>
					</div>
				</div>
			</aside>

			<aside>
				<h2>Poslední komentáře</h2>
				<div class="block">
					<div class="item last-comment">
						<div class="image-wrap">
							<a href="/user/profile/1" data-j-link>
								<img src="/images/user-default-ico.png" alt="User default icon">
							</a>
						</div>

						<span class="name">Lorem</span>

						<blockquote>Lorem ipsum dolor sit amet.</blockquote>
					</div>

					<div class="item last-comment">
						<div class="image-wrap">
							<a href="/user/profile/1" data-j-link>
								<img src="/images/user-default-ico.png" alt="User default icon">
							</a>
						</div>

						<span class="name">Lorem</span>

						<blockquote>
							<a href="/article/1-a-zase-ten-javascript" data-j-link>Lorem ipsum, lorem ipsum dolor sit amet. A dál už si to z hlavy nepamatuji. :(</a>
						</blockquote>
					</div>

					<div class="item last-comment">
						<div class="image-wrap">
							<a href="/user/profile/1" data-j-link>
								<img src="/images/user-default-ico.png" alt="User default icon">
							</a>
						</div>

						<span class="name">Lorem</span>

						<blockquote>Lorem ipsum dolor sit amet.</blockquote>
					</div>
				</div>
			</aside>
		</div>

		<div class="footer-wrap">
			<footer>
				<div class="center-wrap">
					<img src="/images/rjdev-net-logo.png" alt="RJDev.NET">
					<small class="copyright">Copyright &copy; 2015 Roman Jámbor | Všechna práva vyhrazena.</small>
					<div class="back-to-top" data-j-onclick="backToTop()"></div>
				</div>
			</footer>
		</div>
	</body>
</html>
