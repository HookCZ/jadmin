/**
 * Created by Roman Jámbor.
 */

/**
 * @namespace App.SubApps.Admin.Forms
 */

/**
 * Továrna pro formulář na vytváření / editaci stránek
 * @class
 * @memberOf App.SubApps.Admin.Forms
 */
var PageFormFactory = {
	/**
	 * Sestaví formulář
	 * @returns {Jumbo.Form.Form}
	 */
	build: function(callback) {
		var self = this;
		var form = new Jumbo.Form.Form();

		form.addText("name", "Název ").setRequired("Zadejte název stránky.");

		(new App.Models.CategoriesModel()).getAllCategoriesListProForm(function(err, data) {
			form.addSelect("categories", "Kategorie ", data).setRequired("Vyberte kategorie, do kterých má být stránka umístěna").setMultiple(4);
			form.addCheckbox("is_home", "Hlavní stránka ").setAttribute("class", "checkbox");
			form.addCheckbox("visible", "Viditelné ").setChecked().setAttribute("class", "checkbox");
			form.addCheckbox("clanky", "Články <span class='hint' title='Na stránku bude možné vkládat články'>?</span>").setAttribute("class", "checkbox");
			form.addTextArea("text", "Obsah stránky");

			form.addSubmit("send", "Vytvořit");

			form.onSend = self.onSend;

			callback(form);
		});
	},


	/**
	 * Akce provedená on success
	 * @param fields
	 * @param files
	 * @param {Jumbo.Base.BasePresenter} pres
	 * @param actionCall
	 */
	onSend: function(fields, files, pres, actionCall) {
		var id = pres.request.params[0];
		var model = new App.SubApps.Admin.Models.PagesModel();

		var finalize = function(err) {
			if (err instanceof Error) {
				if (pres.isAjax()) {
					pres.returnJSON({formErrorMessages: ["Chyba při vytváření stránky, zkuste to prosím znovu."]});
					return;
				}

				pres.template.formErrorMessages = ["Chyba při vytváření stránky, zkuste to prosím znovu."];
				actionCall();
				return;
			}

			if (pres.isAjax()) {
				pres.returnJSON({redirect: pres.link("Admin:Pages::show")});
				return;
			}

			pres.request.redirect("Admin:Pages::show");
		};


		if (id) {
			model.updatePage(id, fields['name'], fields['text'], fields['categories'], fields['visible'], fields['clanky'], fields['is_home'], finalize);
		} else {
			model.createNewPage(fields['name'], fields['text'], fields['categories'], fields['visible'], fields['clanky'], fields['is_home'], finalize);
		}
	}
};

module.exports = PageFormFactory;