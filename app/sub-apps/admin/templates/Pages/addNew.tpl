{block contentHead}
	<div>
		<img src="/images/admin/icons/dark-home.png" alt=""> Nástěnka | {use title}
	</div>
	<h2>{define title}Přidání stránky{/define}</h2>
	<h3>Stránka je textová položka. Vkládá se do kategorií. Může obsahovat články nebo jen statický text.</h3>
{/block}

{block content}
	<div class="white-block">
		<div class="head">
			<h2>Formulář pro vytvoření nové stránky</h2>
		</div>
		{form newPage}
	</div>
{/block}