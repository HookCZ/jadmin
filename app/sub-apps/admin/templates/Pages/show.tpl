{block contentHead}
	<div>
		<img src="/images/admin/icons/dark-home.png" alt=""> Nástěnka | {use title}
	</div>
	<h2>{define title}Přehled stránek{/define}</h2>
	<h3>Stránka je textová položka. Vkládá se do kategorií. Může obsahovat články nebo jen statický text.</h3>
{/block}

{block content}
	<div class="white-block">
		<div class="head">
			<h2>Tabulka s přehledem vytvořených stránek</h2>
		</div>
		<table>
			<thead>
			<tr>
				<td>Název</td>
				<td>Kategorie</td>
				<td>Viditelné</td>
				<td>Akce</td>
			</tr>
			</thead>
			<tbody>
			{foreach $pages as $page}
				<tr>
					<td data-th-text="Název">{$page['name']}</td>
					<td data-th-text="Kategorie">&nbsp;
						{foreach $page['categories'] as $kat}
							<a href="{link Categories::edit::$kat['category_id']}" data-j-link>{$kat['name']}</a><br>
						{/foreach}
					</td>
					<td data-th-text="Viditelné"><input type="checkbox" class="checkbox" data-j-onchange="changePageVisibility({$page['page_id']})" {if $page['visible'] == 1}checked{/if}></td>
					<td data-th-text="Akce"><a href="{link Pages::edit::$page['page_id']}" data-j-link>Upravit</a> / <a href="{link Pages::delete::$page['page_id']}" data-j-onclick="confirmDelete('Opravdu chcete stránku smazat?')">Smazat</a></td>
				</tr>
			{/foreach}
			</tbody>
		</table>
	</div>
{/block}