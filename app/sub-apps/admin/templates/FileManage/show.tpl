{block contentHead}
	<div>
		<img src="/images/admin/icons/dark-home.png" alt=""> Nástěnka | {use title}
	</div>
	<h2>{define title}Správce souborů{/define}</h2>
	<h3>Správce souborů umožňuje nahrávání, manipulaci a mazání obrázků, dokumentů či jiných souborů</h3>
{/block}

{block content}
	<div class="white-block">
		{*<div class="head">*}
		{*<h2>Tabulka s přehledem vytvořených katalogů</h2>*}
		{*</div>*}
	</div>
{/block}