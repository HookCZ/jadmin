{block contentHead}
	<div>
		<img src="/images/admin/icons/dark-home.png" alt=""> Nástěnka | {use title}
	</div>
	<h2>{define title}Editace menu{/define}</h2>
	<h3>Menu je základní položka, do kterého se vše umisťuje.</h3>
{/block}

{block content}
	<div class="white-block">
		<div class="head">
			<h2>Formulář pro upravení menu</h2>
		</div>
		{form newMenu}
	</div>
{/block}