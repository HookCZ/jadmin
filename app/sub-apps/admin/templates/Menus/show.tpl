{block contentHead}
	<div>
		<img src="/images/admin/icons/dark-home.png" alt=""> Nástěnka | {use title}
	</div>
	<h2>{define title}Přehled menu{/define}</h2>
	<h3>Menu je základní položka, do kterého se vše umisťuje.</h3>
{/block}

{block content}
	<div class="white-block">
		<div class="head">
			<h2>Tabulka s přehledem vytvořených menu</h2>
		</div>
		<table>
			<thead>
			<tr>
				<th># ID</th>
				<td>Název</td>
				<td>Pozice</td>
				<td>Viditelné</td>
				<td>Akce</td>
			</tr>
			</thead>
			<tbody>
			{foreach $menus as $menu}
				<tr>
					<th data-th-text="# ID">{$menu['menu_id']}</th>
					<td data-th-text="Název">{$menu['name']}</td>
					<td data-th-text="Pozice">{$menusPositions[$menu['position']]}</td>
					<td data-th-text="Viditelné"><input type="checkbox" class="checkbox" data-j-onchange="changeMenuVisibility({$menu['menu_id']})" {if $menu['visible'] == 1}checked{/if}></td>
					<td data-th-text="Akce"><a href="{link Menus::edit::$menu['menu_id']}" data-j-link>Upravit</a> / <a href="{link Menus::delete::$menu['menu_id']}" data-j-onclick="confirmDelete('Opravdu chcete menu smazat?')">Smazat</a></td>
				</tr>
			{/foreach}
			</tbody>
		</table>
	</div>
{/block}