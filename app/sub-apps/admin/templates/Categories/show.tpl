{block contentHead}
	<div>
		<img src="/images/admin/icons/dark-home.png" alt=""> Nástěnka | {use title}
	</div>
	<h2>{define title}Přehled kategorií{/define}</h2>
	<h3>Kategorie je položka, která se umisťuje do menu a umožňuje vytváření stromové struktury.</h3>
{/block}

{block content}
	<div class="white-block">
		<div class="head">
			<h2>Tabulka s přehledem vytvořených kategorií</h2>
		</div>
		<table>
			<thead>
			<tr>
				<td>Název</td>
				<td>Nadřazená kategorie</td>
				<td>Menu</td>
				<td>Viditelné</td>
				<td>Hlavní</td>
				<td>Akce</td>
			</tr>
			</thead>
			<tbody>
			{foreach $categories as $category}
				<tr>
					<td data-th-text="Název">{$category['name']}</td>
					<td data-th-text="Nadřazená kategorie">
						{if $category['parent']}
							<a href="{link Categories::edit::$category['parent']}" data-j-link>{$category['parent_name']}</a>
						{else}
							Výchozí
						{/if}
					</td>
					<td data-th-text="Menu"><a href="{link Menus::edit::$category['menu_id']}" data-j-link>{$category['menu_name']}</a></td>
					<td data-th-text="Viditelné"><input type="checkbox" class="checkbox" data-j-onchange="changeCategoryVisibility({$category['category_id']})" {if $category['visible'] == 1}checked{/if}></td>
					<td data-th-text="Hlavní">{if $category['hlavni'] == 1}Ano{else}Ne{/if}</td>
					<td data-th-text="Akce"><a href="{link Categories::edit::$category['category_id']}" data-j-link>Upravit</a> / <a href="{link Categories::delete::$category['category_id']}" data-j-onclick="confirmDelete('Opravdu chcete kategorii smazat?')">Smazat</a></td>
				</tr>
			{/foreach}
			</tbody>
		</table>
	</div>
{/block}