{block contentHead}
	<div>
		<img src="/images/admin/icons/dark-home.png" alt=""> Nástěnka | {use title}
	</div>
	<h2>{define title}Přidání kategorie{/define}</h2>
	<h3>Kategorie je položka, která se umisťuje do menu a umožňuje vytváření stromové struktury. Kategorie může být označena jako hlavní. To způsobí přímé vypsání jejích položek.</h3>
{/block}

{block content}
	<div class="white-block">
		<div class="head">
			<h2>Formulář pro vytvoření nové kategorie</h2>
		</div>
		{form newCategory}
	</div>
{/block}