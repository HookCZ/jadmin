<!DOCTYPE html>
<html {if $lang}lang="{$lang}"{/if}>
	<head>
		<meta charset="utf-8">
		<meta name='viewport' content='width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;'>


		<title>{defined title}{use title} | {/defined}Jadmin</title>


		{*<link rel="stylesheet" href="/jumbo-lib/jumbo.css">*}
		{*<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>*}
		{*<link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>*}
		<link rel="stylesheet" href="/rjlib/default-style.css">
		<link rel="stylesheet" href="/css/admin.css">
		<link rel='stylesheet' media='(max-width: 8in)' href="/css/admin-mobile.css">


		{*<script src="/js/jquery.js"></script>*}
		<script src="/rjlib/base/core.js"></script>
		<script src="/rjlib/base/xhr.js"></script>
		<script src="/rjlib/base/app.js"></script>
		<script src="/rjlib/base/dom.js"></script>
		<script src="/rjlib/base/effects.js"></script>
		<script src="/rjlib/base/_class.js"></script>
		<script src="/rjlib/base/Dialog.js"></script>
		<script src="/rjlib/base/windows.js"></script>
		<script src="/rjlib/base/prototype-edit.js"></script>

		<script src="/js/admin/script.js"></script>


		{include head}


		<script>
			{if $errorMessages}
				{foreach $errorMessages as $message}
					alert("{$message}");
				{/foreach}
			{/if}
		</script>



		{* Froala editor *}
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="/froala/css/froala_editor.css">
		<link rel="stylesheet" href="/froala/css/froala_style.css">
		<link rel="stylesheet" href="/froala/css/plugins/code_view.css">
		<link rel="stylesheet" href="/froala/css/plugins/colors.css">
		<link rel="stylesheet" href="/froala/css/plugins/emoticons.css">
		<link rel="stylesheet" href="/froala/css/plugins/image_manager.css">
		<link rel="stylesheet" href="/froala/css/plugins/image.css">
		<link rel="stylesheet" href="/froala/css/plugins/line_breaker.css">
		<link rel="stylesheet" href="/froala/css/plugins/table.css">
		<link rel="stylesheet" href="/froala/css/plugins/char_counter.css">
		<link rel="stylesheet" href="/froala/css/plugins/video.css">
		<link rel="stylesheet" href="/froala/css/plugins/fullscreen.css">
		<link rel="stylesheet" href="/froala/css/plugins/file.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="/froala/js/froala_editor.min.js"></script>
		<script src="/froala/js/plugins/align.min.js"></script>
		<script src="/froala/js/plugins/char_counter.min.js"></script>
		<script src="/froala/js/plugins/code_view.min.js"></script>
		<script src="/froala/js/plugins/colors.min.js"></script>
		<script src="/froala/js/plugins/emoticons.min.js"></script>
		<script src="/froala/js/plugins/entities.min.js"></script>
		<script src="/froala/js/plugins/file.min.js"></script>
		<script src="/froala/js/plugins/font_family.min.js"></script>
		<script src="/froala/js/plugins/font_size.min.js"></script>
		<script src="/froala/js/plugins/fullscreen.min.js"></script>
		<script src="/froala/js/plugins/image.min.js"></script>
		<script src="/froala/js/plugins/image_manager.min.js"></script>
		<script src="/froala/js/plugins/inline_style.min.js"></script>
		<script src="/froala/js/plugins/line_breaker.min.js"></script>
		<script src="/froala/js/plugins/link.min.js"></script>
		<script src="/froala/js/plugins/lists.min.js"></script>
		<script src="/froala/js/plugins/paragraph_format.min.js"></script>
		<script src="/froala/js/plugins/paragraph_style.min.js"></script>
		<script src="/froala/js/plugins/quote.min.js"></script>
		<script src="/froala/js/plugins/save.min.js"></script>
		<script src="/froala/js/plugins/table.min.js"></script>
		<script src="/froala/js/plugins/url.min.js"></script>
		<script src="/froala/js/plugins/video.min.js"></script>

		<script src='/froala/js/languages/cs.js'></script>
	</head>
	<body id="app">
		<div class="top-panel">
			<div class="logo">
				<img src="/images/admin/logo.png" alt="Jadmin">
			</div>
			<div class="right">
				<div class="user">
					<img src="/images/admin/icons/user.png" alt="">
					<span class="user-name">Lorem Ipsum</span>
				</div>
			</div>
		</div>

		<div class="side-panel">
			<nav>
				<ul>
					{* NÁSTĚNKA *}
					<li><a href="{link Main::show}" data-j-link><span><img src="/images/admin/icons/home.png" alt=""></span>Nástěnka</a></li>

					{* MENU *}
					<li>
						<a data-j-onclick="toggleRoll()"><span><img src="/images/admin/icons/menu.png" alt=""></span>Menu</a>
						<ul>
							<li><a href="{link Menus::show}" data-j-link>Přehled menu</a></li>
							<li><a href="{link Menus::addNew}" data-j-link>Nové menu</a></li>
						</ul>
					</li>

					{* KATEGORIE *}
					<li>
						<a data-j-onclick="toggleRoll()"><span><img src="/images/admin/icons/category.png" alt=""></span>Kategorie</a>
						<ul>
							<li><a href="{link Categories::show}" data-j-link>Přehled kategorií</a></li>
							<li><a href="{link Categories::addNew}" data-j-link>Nová kategorie</a></li>
						</ul>
					</li>

					{* STRÁNKY *}
					<li>
						<a data-j-onclick="toggleRoll()"><span><img src="/images/admin/icons/page.png" alt=""></span>Stránky</a>
						<ul>
							<li><a href="{link Pages::show}" data-j-link>Přehled stránek</a></li>
							<li><a href="{link Pages::addNew}" data-j-link>Nová stránka</a></li>
						</ul>
					</li>

					{* ČLÁNKY *}
					<li><a href="#" onclick="jumbo.alert('Zatím neimplementováno'); return false;"><span><img src="/images/admin/icons/article.png" alt=""></span>Články</a></li>

					{* KATALOGY *}
					<li>
						<a data-j-onclick="toggleRoll()"><span><img src="/images/admin/icons/catalog.png" alt=""></span>Katalogy</a>
						<ul>
							<li><a href="{link Catalogs::show}" data-j-link>Přehled katalogů</a></li>
							<li><a href="{link Catalogs::addNew}" data-j-link>Nový katalog</a></li>
						</ul>
					</li>

					{* PRODUKTY *}
					<li>
						<a data-j-onclick="toggleRoll()"><span><img src="/images/admin/icons/product.png" alt=""></span>Produkty</a>
						<ul>
							<li><a href="{link Products::show}" data-j-link>Přehled produktů</a></li>
							<li><a href="{link Products::addNew}" data-j-link>Nový produkt</a></li>
							<li><a href="{link Products::showDPH}" data-j-link>Sazby DPH</a></li>
							<li><a href="{link Products::showZaruky}" data-j-link>Záruční doby</a></li>
						</ul>
					</li>

					{* OBJEDNÁVKY *}
					<li><a href="#" onclick="jumbo.alert('Zatím neimplementováno'); return false;"><span><img src="/images/admin/icons/orders.png" alt=""></span>Objednávky</a></li>

					{* GALERIE *}
					<li>
						<a data-j-onclick="toggleRoll()"><span><img src="/images/admin/icons/galery.png" alt=""></span>Galegie</a>
						<ul>
							<li><a href="{link Galleries::show}" data-j-link>Přehled galerií</a></li>
							<li><a href="{link Galleries::addNew}" data-j-link>Nová galerie</a></li>
						</ul>
					</li>

					{* SPRÁVCE SOUBORŮ *}
					<li><a href="{link FileManage::show}" data-j-link><span><img src="/images/admin/icons/file-manager.png" alt=""></span>Správce souborů</a></li>

					{* NASTAVENÍ *}
					<li>
						<a data-j-onclick="toggleRoll()"><span><img src="/images/admin/icons/settings.png" alt=""></span>Nastavení</a>
						<ul>
							<li><a href="{link Settings::show}" data-j-link>Základní nastavení</a></li>
							<li><a href="{link Settings::routes}" data-j-link>Routování</a></li>
							<li><a href="{link Settings::eshop}" data-j-link>Eshop</a></li>
							<li><a href="{link Settings::emails}" data-j-link>Odchozí emaily</a></li>
						</ul>
					</li>
				</ul>
			</nav>
		</div>

		<div class="content" data-j-content>
			<div class="head">
				{include contentHead}
			</div>

			<div class="body">
				{include content}
			</div>

			<script>
				jumbo.replaceCheckboxes(".checkbox");


				$(function() {
					$('textarea').each(function(i, el) {
						el = $(el);
						el.froalaEditor({
							toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', '|', 'color', 'emoticons', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'quote', 'insertHR', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html'],
							language: "cs", // TODO: Reagovat na nastavený jazyk v administraci
							entities: ""
						}).on('froalaEditor.blur', function () {
							el.val(el.froalaEditor("html.get"));
						});

						// Pod textareu dáme btn na vypnutí froaly
						$("<span class='close-editor-btn'>Disable editor</span>").on("click", function () {
							el.froalaEditor('destroy');
							$(this).remove();
						}).insertAfter(el);
					});

					$("textarea").prev().find("a").each(function () {
						if (this.href == "https://froala.com/wysiwyg-editor") {
							this.remove();
						}
					});
				});
			</script>
		</div>
	</body>
</html>
