{nolayout}

<!DOCTYPE html>
<html {if $lang}lang="{$lang}"{/if}>
	<head>
		<meta charset="utf-8">
		<meta name='viewport' content='width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;'>


		<title>Sign in | Jadmin</title>


		<link rel="stylesheet" href="/rjlib/default-style.css">
		<link rel="stylesheet" href="/css/admin-sign.css">
		{*<link rel='stylesheet' media='(max-width: 8in)' href="/css/admin-sign-mobile.css">*}


		<script src="/rjlib/base/core.js"></script>
		<script src="/rjlib/base/xhr.js"></script>
		<script src="/rjlib/base/app.js"></script>
		<script src="/rjlib/base/dom.js"></script>
		<script src="/rjlib/base/effects.js"></script>
		<script src="/rjlib/base/_class.js"></script>
		<script src="/rjlib/base/Dialog.js"></script>
		<script src="/rjlib/base/windows.js"></script>
		{*<script src="/rjlib/base/prototype-edit.js"></script>*}

		<script src="/js/admin/script.js"></script>
		<script src="/js/md5.js"></script>

		<script>
			{if $errorMessages}
				{foreach $errorMessages as $message}
					jumbo.alert("{$message}");
				{/foreach}
			{/if}
		</script>
	</head>
	<body> {* APP *}
		<div class="wrap">
			<div class="content" data-j-content>
				<img src="/images/admin/logo.png" alt="Jadmin">

				{customform signInForm}
					<input type="text" name="email" placeholder="Email...">
					<input type="password" name="password" placeholder="Heslo..." onchange="this.nextElementSibling.value = md5(this.value)">
					<input type="hidden" name="pass_hash">
					<input type="submit" name="send" value="Sign In" class="sign-in-btn">
				{/customform}

				{*<script>*}
					{*jumbo.replaceCheckboxes(".checkbox");*}
				{*</script>*}
			</div>

			{if $formErrorMessages}
				<p>
					{foreach $formErrorMessages as $message}
						<span class="form-error">{$message}</span><br>
					{/foreach}
				</p>
			{/if}

		</div>
	</body>
</html>
