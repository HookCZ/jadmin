{block contentHead}
	<div>
		<img src="/images/admin/icons/dark-home.png" alt=""> Nástěnka | {use title}
	</div>
	<h2>{define title}Přehled katalogů{/define}</h2>
	<h3>Katalog je položka, která shromažďuje produkty do skupiny, která pak lze umístit do kategorie.</h3>
{/block}

{block content}
	<div class="white-block">
		<div class="head">
			<h2>Tabulka s přehledem vytvořených katalogů</h2>
		</div>
	</div>
{/block}