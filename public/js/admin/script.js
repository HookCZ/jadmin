/**
 * Created by Roman Jámbor.
 */

jumbo.createController("body",
	/**
	 * @param {jumbo.xhr} $xhr
	 */
	function($xhr) {
		/**
		 * Onclick na položkách menu
		 * @param el
		 */
		this.toggleRoll = function(el) {
			var childUl = el.parentNode.getElementsByTagName("ul")[0];

			if (childUl) {
				if (childUl.offsetHeight == 0) {
					childUl.unroll();
					el.setClass("opened");
				} else {
					childUl.roll();
					el.setClass("");
				}
			}
		};


		/**
		 * DELETE s potvrzením
		 * @param text
		 * @param el
		 * @param e
		 */
		this.confirmDelete = function(text, el, e) {
			var self = this;
			e.preventDefault();

			jumbo.confirm(text, ["Ano", "Ne"], function(moznost) {
				if (moznost == "Ano") {
					$xhr.delete(el.href, function(err, data) {
						if (data.error || err) {
							jumbo.alert(err.message || data.error);
						} else if (data.redirect) {
							self.loadPage(data.redirect);
						}
					}, "json");
				}
			});
		};


		/**
		 * Zpracovává ajaxové checkboxy
		 * @param url
		 * @param el
		 */
		this.changeCheckboxState = function(url, el) {
			var checkbox = el.nextSibling;
			var chControl = jumbo.checkboxControl(checkbox);

			// Prozatím zamezíme změně
			el.checked = !el.checked;
			el.checked ? chControl.check() : chControl.uncheck();

			// Vypneme checkbox
			chControl.disable();

			$xhr.put(url + "?state=" + (el.checked ? 0 : 1), null, function(err, data) {
				chControl.enable();

				if (!err && !data.error) {
					el.checked ? chControl.uncheck() : chControl.check();
					el.checked = !el.checked;
				}
			})
		};


		/**
		 * Změna stavu viditelnosti menu
		 * @param id
		 * @param el
		 */
		this.changeMenuVisibility = function(id, el) {
			this.changeCheckboxState("/menus/changevisibility/" + id, el);
		};


		this.changeCategoryVisibility = function(id, el) {
			this.changeCheckboxState("/categories/changevisibility/" + id, el);
		};


		this.changePageVisibility = function(id, el) {
			this.changeCheckboxState("/pages/changevisibility/" + id, el);
		};
	}
);