/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.net                                 *
 ***************************************************
 * FileName: ColorPicker.js                  ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * @requires math.js, dinamics.js
 */

/**
 * Grafický výběr barvy
 * @param {Node} wrapEl Element do kterého bude colorPicker vytvořen
 * @constructor
 */
jumbo.ColorPicker = function(wrapEl, callback) {
	if (!jumbo.isNode(wrapEl)) {
		if (jumbo.consoleLogging == true) console.log("Objekt dosazený pro vykreslování je chybný.");
		return;
	}

	// Uložíme si obalový element
	this.wrap = wrapEl;

	// Zapíšeme callback
	this.callback = callback;

	// Výchozí barva
	this.barva = [255, 0, 0];//[0, 0, 255];

	this.aktualniBarva = [0, 0, 255];

	// Absolutní souřadnice wrap elementu
	this.wrapPos = null;

	// Sestavíme HTML část colorPickeru
	this.sestavit();
};

/**
 * Sestaví HTML část colorPickeru
 */
jumbo.ColorPicker.prototype.sestavit = function() {
	// Vytvoříme canvas na kruhový výběr
	this.colorPickCanvas = jumbo.create("canvas", {
		width:	220,
		height:	220,
		class:	"kruhovy-vyber"
	});

	// Vložíme obrázek kruhu
	var ctx = this.colorPickCanvas.getContext('2d');
	var img = new Image();
	img.src = "./images/color-pick-kruh.png";
	img.onload = function() {
		ctx.drawImage(img, 0, 0, img.width, img.height);
	};

	var self = this;

	// Výběrový pointer
	this.colorPickPointer = jumbo.addEvent(jumbo.create("div", {
		class:	"barva-pointer"
	}), "mousedown", function(e) {
		e = e || window.event;
		startPos = jumbo.absPos(self.colorPickCanvas);

		var mouseMoveCallee = null;
		jumbo.addEvent(document, "mousemove", mouseMoveCallee = function(e) {
			e = e || window.event;

			if (mouseMoveCallee == null) {
				mouseMoveCallee = arguments.callee;
			}

			// Vypočteme nové souřadnice umístění pointru
			// Vytvoříme si vektor od středu k pozici myši
			var cur = jumbo.curPos(e);
			var stred = {
				x: /*self.colorPickCanvasPos*/startPos.x + 110,
				y: /*self.colorPickCanvasPos*/startPos.y + 110
			};
			var vCur = {
				x: cur.x - stred.x,
				y: cur.y - stred.y
			};

			// Vytvoříme vektor od středu doprava
			var vHor = {
				x: 110,
				y: 0
			};

			// Úhel svíraný vektory
			var uhel = jumbo.vectsAngle(vHor, vCur);

			// souřadnice pointru
			var p = {
				x: 97 * Math.cos(uhel * .017453292519943295),
				y: 97 * Math.sin(uhel * .017453292519943295)
			};

			p.x += 110;
			p.y += 110;

			// Umístíme pointer na pozici
			self.colorPickPointer.style.margin = (p.y - 218 - 13) + "px 0 0 " + (p.x - 11) + "px";

			self.zjistiBarvu(p.x, p.y);
		});

		jumbo.addEvent(document, "mouseup", function() {
			jumbo.removeEvent(document, "mousemove", mouseMoveCallee);
			jumbo.removeEvent(document, "mouseup", arguments.callee);
		});
	});

	// Event pro telefony
	try {
		jumbo.addEvent(this.colorPickPointer, "touchstart", function(e) {
			e = e || window.event;
			startPos = jumbo.absPos(self.colorPickCanvas);

			var mouseMoveCallee = null;
			jumbo.addEvent(document, "touchmove", mouseMoveCallee = function(e) {
				e = e || window.event;
				e.preventDefault();

				if (mouseMoveCallee == null) {
					mouseMoveCallee = arguments.callee;
				}

				if (e.touches.length != 1) {
					return;
				}

				var touch = e.touches[0];

				// Vypočteme nové souřadnice umístění pointru
				// Vytvoříme si vektor od středu k pozici myši
				var cur = {
					x:	touch.clientX,
					y:	touch.clientY
				};
				var stred = {
					x: /*self.colorPickCanvasPos*/startPos.x + 110,
					y: /*self.colorPickCanvasPos*/startPos.y + 110
				};
				var vCur = {
					x: cur.x - stred.x,
					y: cur.y - stred.y
				};

				// Vytvoříme vektor od středu doprava
				var vHor = {
					x: 110,
					y: 0
				};

				// Úhel svíraný vektory
				var uhel = jumbo.vectsAngle(vHor, vCur);

				// souřadnice pointru
				var p = {
					x: 97 * Math.cos(uhel * .017453292519943295),
					y: 97 * Math.sin(uhel * .017453292519943295)
				};

				p.x += 110;
				p.y += 110;

				// Umístíme pointer na pozici
				self.colorPickPointer.style.margin = (p.y - 218 - 13) + "px 0 0 " + (p.x - 11) + "px";

				self.zjistiBarvu(p.x, p.y);
			});

			jumbo.addEvent(document, "touchend", function() {
				jumbo.removeEvent(document, "touchmove", mouseMoveCallee);
				jumbo.removeEvent(document, "touchend", arguments.callee);
			});
		});
	} catch (ex) { }

	/**********************************************************************************
	 * Canvas pro výběr bílé
	 */
	this.whitePickCanvas = jumbo.create("canvas", {
		width:	76,//220,
		height:	158,//20,
		class:	"vyber-bile"
	});

	// Necháme sestavit obrázek pro výchozí barvu
	this.changeWhitePicker(this.barva[0], this.barva[1], this.barva[2]);

	// Nastavíme event na výběr
	jumbo.addEvent(this.whitePickCanvas, "mousedown", function(e) {
		// Zjistíme barvu už při kliku, pak až při pohybu
		e = e || window.event;
		self.zjistiBilou(e);

		var mouseMoveCallee = null;
		jumbo.addEvent(document, "mousemove", mouseMoveCallee = function(e) {
			if (mouseMoveCallee == null) {
				mouseMoveCallee = arguments.callee;
			}
			self.zjistiBilou(e);
		});

		jumbo.addEvent(document, "mouseup", function() {
			document.removeEvent("mousemove", mouseMoveCallee);
			document.removeEvent("mouseup", arguments.callee);
		});
	});

	/**********************************************************************************
	 * Canvas pro výběr černé
	 */
	this.blackPickCanvas = jumbo.create("canvas", {
		width:	76,//70,
		height:	158,//148,
		class:	"vyber-cerne"
	});

	// Necháme sestavit obrázek pro výchozí barvu
	this.changeBlackPicker(this.barva[0], this.barva[1], this.barva[2]);

	// Nastavíme event na výběr
	jumbo.addEvent(this.blackPickCanvas, "mousedown", function(e) {
		// Zjistíme barvu už při kliku, pak až při pohybu
		e = e || window.event;
		self.zjistiCernou(e);

		var mouseMoveCallee = null;
		jumbo.addEvent(document, "mousemove", mouseMoveCallee = function(e) {
			if (mouseMoveCallee == null) {
				mouseMoveCallee = arguments.callee;
			}
			self.zjistiCernou(e);
		});

		jumbo.addEvent(document, "mouseup", function() {
			document.removeEvent("mousemove", mouseMoveCallee);
			document.removeEvent("mouseup", arguments.callee);
		});
	});

	/**********************************************************************************
	 * Náhledový box
	 */
	this.nahled = jumbo.create("svg", {
		width:		"51",
		height:		"108",
		class:		"nahled-vybrane",
		//style:	"background: rgb(" + this.barva[0] + ", " + this.barva[1] + ", " + this.barva[2] + ")"
		//M51,0 A51,54 0 0,0 51,108 L51,88 A15,17.5 0 0,1 51,20 Z
		content:	'<path d="M51,0 A51,54 0 0,0 51,108 L51,80 A31,24 0 0,1 51,28 Z" fill="rgb(' + this.barva[0] + ', ' + this.barva[1] + ', ' + this.barva[2] + ')" />'
	});

	/**********************************************************************************
	 * Náhled aktuálně vybrané barvy
	 */
	this.nahledAktualniBarva = jumbo.create("svg", {
		width:		"51",
		height:		"108",
		class:		"nahled-aktualni",
		// M0,0 A51,54 0 0,1 0,108 L0,80 A13,10 0 0,0 0,28 Z
		// M0,0 A51,54 0 0,1 0,108 L0,88 A15,17.5 0 0,0 0,20 Z
		content:	'<path d="M0,0 A51,54 0 0,1 0,108 L0,80 A31,24 0 0,0 0,28 Z" fill="rgb(' + this.aktualniBarva[0] + ', ' + this.aktualniBarva[1] + ', ' + this.aktualniBarva[2] + ')" />'
	});

	/**********************************************************************************
	 * Výběrové tlačítko
	 */
	var potvrditBtn = jumbo.addEvent(jumbo.create("button", {
		class:		"color-picker-confirm",
		content:	"Vybrat"
	}), "click", function() {
		self.potvrditVyber();
	});

	/**********************************************************************************
	 * colorPicker vlastní wrap
	 */
	this.cpWrap = jumbo.create("div", {
		class:		"jumbo-color-picker-wrap",
		content:	[
			this.colorPickCanvas,
			this.whitePickCanvas,
			this.blackPickCanvas,
			this.nahled,//nahledWrap,
			this.nahledAktualniBarva,//aktualniBarvaWrap,
			this.colorPickPointer,
			potvrditBtn
		]
	});

	this.wrap.appendChild(this.cpWrap);

	//this.wrapPos = jumbo.absPos(this.cpWrap);
	//this.colorPickCanvasPos = jumbo.absPos(self.colorPickCanvas);

	this.setColorPickPointerPos();
};

/**
 * Nastavení ukazatele na uloženou barvu this.barva
 */
jumbo.ColorPicker.prototype.setColorPickPointerPos = function() {
	// Převedeme barvu na HSL a složku H přepočteme na úhel
	var hsl = jumbo.rgbToHSL(this.barva[0], this.barva[1], this.barva[2]);
	var uhel = hsl[0] * 2 * 3.14159 * 57.29577951308232; // H * 2 * PI -> *57.xxx -> převod na stupně z radiánů

	// souřadnice pointru
	var p = {
		x: 97 * Math.cos(uhel * .017453292519943295),
		y: 97 * Math.sin(uhel * .017453292519943295)
	};

	p.x += 110;
	p.y += 110;

	// Umístíme pointer na pozici
	this.colorPickPointer.style.margin = (p.y - 218 - 13) + "px 0 0 " + (p.x - 11) + "px";
};

/**
 * Zjistí barvu na kterou bylo v kruhu kliknuto
 * @param canvasX
 * @param canvasY
 */
jumbo.ColorPicker.prototype.zjistiBarvu = function(canvasX, canvasY) {
	/*e = e || window.event;
	var cur = jumbo.curPos(e);

	var pos = jumbo.absPos(this.colorPickCanvas);//this.colorPickCanvas.absPos();
	var canvasX = Math.floor(cur.x - pos.x);
	var canvasY = Math.floor(cur.y - pos.y);*/
	var imageData = this.colorPickCanvas.getContext('2d').getImageData(canvasX, canvasY, 1, 1);

	// Najeli jsme myší mimo a vybrala se tam černá,.. to nechceme
	if (imageData.data[0] == 0 && imageData.data[1] == 0 && imageData.data[2] == 0) {
		return;
	}

	this.vybratBarvu(imageData.data[0], imageData.data[1], imageData.data[2]);

	// Barvu máme, teď upravíme výběr bílé
	this.changeWhitePicker(imageData.data[0], imageData.data[1], imageData.data[2]);

	// Upravíme výběr černé
	this.changeBlackPicker(imageData.data[0], imageData.data[1], imageData.data[2]);
};

/**
 * Zjistí prvek bílé u vybrané barvy
 * @param e Event
 */
jumbo.ColorPicker.prototype.zjistiBilou = function(e) {
	e = e || window.event;

	var t = jumbo.eventTarget(e);

	if (t != this.whitePickCanvas) {
		return;
	}

	var cur = jumbo.curPos(e);
	var pos = jumbo.absPos(this.whitePickCanvas);//.absPos();
	var canvasX = Math.floor(cur.x - pos.x);
	var canvasY = Math.floor(cur.y - pos.y);
	var imageData = this.whitePickCanvas.getContext('2d').getImageData(canvasX, canvasY, 1, 1);

	// Najeli jsme myší mimo a vybrala se tam černá,.. to nechceme
	if (imageData.data[0] == 0 && imageData.data[1] == 0 && imageData.data[2] == 0) {
		return;
	}

	this.vybratBarvu(imageData.data[0], imageData.data[1], imageData.data[2]);

	// Upravíme výběr černé
	this.changeBlackPicker(imageData.data[0], imageData.data[1], imageData.data[2]);
};

/**
 * Zjistí prvek černé u vybrané barvy
 * @param e Event
 */
jumbo.ColorPicker.prototype.zjistiCernou = function(e) {
	e = e || window.event;

	var t = jumbo.eventTarget(e);

	if (t != this.blackPickCanvas) {
		return;
	}

	var cur = jumbo.curPos(e);
	var pos = jumbo.absPos(this.blackPickCanvas);//.absPos();
	var canvasX = Math.floor(cur.x - pos.x);
	var canvasY = Math.floor(cur.y - pos.y);
	var imageData = this.blackPickCanvas.getContext('2d').getImageData(canvasX, canvasY, 1, 1);
	this.vybratBarvu(imageData.data[0], imageData.data[1], imageData.data[2]);
};


/**
 * Překreslí pruh s výběrem bílé
 * @param r Červená decimálně
 * @param g Zelená
 * @param b Modrá
 */
jumbo.ColorPicker.prototype.changeWhitePicker = function(r, g, b) {
//	var svgData = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.2" width="220px" height="80px">' +
//		'<defs>' +
//		'<linearGradient id="whiteOverlay" x1="0%" y1="0%" x2="100%" y2="0%">' +
//		'<stop offset="10%" style="stop-color:rgb(255,255,255);stop-opacity:0" />' +
//		'<stop offset="90%" style="stop-color:rgb(255,255,255);stop-opacity:1" />' +
//		'</linearGradient>' +
//		'</defs>' +
//		'<rect width="220px" height="20px" fill="rgb(' + r + ', ' + g + ', ' + b + ')" />' +
//		'<rect width="220px" height="20px" fill="url(#whiteOverlay)" />' +
//		'<image xlink:href="images/color-pick-prechod-bile.png" width="220px" height="20px" />' +
//		'</svg>';

	var svgData = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.2" width="76" height="158">' +
		'<defs>' +
			'<linearGradient id="whiteOverlay" x1="0%" y1="0%" x2="0%" y2="100%">' +
				'<stop offset="16%" style="stop-color:rgb(' + r + ', ' + g + ', ' + b + ');stop-opacity:1" />' +
				'<stop offset="84%" style="stop-color:rgb(255,255,255);stop-opacity:1" />' +
			'</linearGradient>' +
		'</defs>' +
		'<path d="M76,0 A76,79 0 0,0 76,158 L76,138 A56,59 0 0,1 76,20 Z" fill="url(#whiteOverlay)" />' +
		'</svg>';

//	var DOMURL = window.URL || window.webkitURL || window;

//	var svg = new Blob([svgData], {type: 'image/svg+xml;charset=utf-8'});
//	var url = window.URL.createObjectURL(svg);


//	document.body.appendChild(jumbo.create("div", {content:svgData2}));


	//<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="220px" height="80px"><rect width="220px" height="20px" style="fill: rgb(' + r + ', ' + g + ', ' + b + ');" /><image xlink:href="images/color-pick-prechod-bile.png" width="220px" height="20px" /></svg>'; //"./images/color-pick-prechod-bile.png";


	//alert(url);

	//url = url.replace("blob:http%3A", "http:")

//	img.src = 'data:image/svg+xml;base64,' + btoa('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="220px" height="80px"><rect width="220px" height="20px" style="fill: rgb(' + r + ', ' + g + ', ' + b + ');" /><image xlink:href="images/color-pick-prechod-bile.png" width="220px" height="20px" /></svg>'); //"./images/color-pick-prechod-bile.png";

//	img.src = url;




	var ctx = this.whitePickCanvas.getContext('2d');
	var img = new Image();
	img.src = 'data:image/svg+xml;charset=utf-8,' + svgData;
	img.onload = function() {
		ctx.drawImage(img, 0, 0, img.width, img.height);
	};
};

/**
 * Překreslí pruh s výběrem černé - jas
 * @param r Červená decimálně
 * @param g Zelená
 * @param b Modrá
 */
jumbo.ColorPicker.prototype.changeBlackPicker = function(r, g, b) {
	var svgData = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.2" width="81" height="158">' +
		'<defs>' +
			'<linearGradient id="blackOverlay" x1="0%" y1="0%" x2="0%" y2="100%">' +
				'<stop offset="16%" style="stop-color:rgb(' + r + ', ' + g + ', ' + b + ');stop-opacity:1" />' +
				'<stop offset="84%" style="stop-color:rgb(0,0,0);stop-opacity:1" />' +
			'</linearGradient>' +
		'</defs>' +
		'<path d="M0,0 A76,79 0 0,1 0,158 L0,138 A56,59 0 0,0 0,20 Z" fill="url(#blackOverlay)" />' +
	'</svg>';

	var ctx = this.blackPickCanvas.getContext('2d');
	var img = new Image();
	img.src = 'data:image/svg+xml;charset=utf-8,' + svgData;
	img.onload = function() {
		ctx.drawImage(img, 0, 0, img.width, img.height);
	};
};
/**
 * Zapíšeme si danou barvu a zobrazíme ji v náhledovém boxu
 * @param r
 * @param g
 * @param b
 */
jumbo.ColorPicker.prototype.vybratBarvu = function(r, g, b) {
	this.barva = [r, g, b];

	// Necháme zobrazit vybranou barvu v boxu
	this.nahled.children[0].style.fill = "rgb(" + r + ", " + g + ", " + b + ")";
};

/**
 * Uložíme barvu do aktuální a zavoláme callback fci
 */
jumbo.ColorPicker.prototype.potvrditVyber = function() {
	this.aktualniBarva = [this.barva[0], this.barva[1], this.barva[2]];
	this.nahledAktualniBarva.children[0].style.fill = "rgb(" + this.barva[0] + ", " + this.barva[1] + ", " + this.barva[2] + ")";

	if (this.callback != null) {
		this.callback(this.barva[0], this.barva[1], this.barva[2]);
	}
};