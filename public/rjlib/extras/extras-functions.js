/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.net                                 *
 ***************************************************
 * FileName: extras-functions.js             ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/


/**
 * Převod RGB modelu na model HSL
 * @param r Červená
 * @param g Zelená
 * @param b Modrá
 * @returns {[h, s, l]}
 */
jumbo.rgbToHSL = function(r, g, b){
	r /= 255;
	g /= 255;
	b /= 255;

	var max = Math.max(r, g, b);
	var min = Math.min(r, g, b);
	var l = (max + min) / 2;
	var h, s;

	if (max == min){
		h = 0;
		s = 0;
	} else {
		var c = max - min;
		s = (l > 0.5) ? (c / (2 - (c))) : (c / (max + min));

		switch (max) {
			case r:
				h = (g - b) / c + ((g < b) ? 6 : 0);
				break;
			case g:
				h = (b - r) / c + 2;
				break;
			case b:
				h = (r - g) / c + 4;
				break;
		}

		h /= 6;
	}

	return [h, s, l];
};


/**
 * Převod HSL formátu na RGB
 * @param h
 * @param s
 * @param l
 * @returns {[r, g, b]}
 */
jumbo.hslToRGB = function (h, s, l) {
	var r, g, b;

	if (s == 0){
		r = g = b = l;
	} else {
		var hue2rgb = function hue2rgb(p, q, t) {
			if (t < 0) t += 1;
			if (t > 1) t -= 1;
			if (t < 1/6) return p + (q - p) * 6 * t;
			if (t < 1/2) return q;
			if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
			return p;
		};

		var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
		var p = 2 * l - q;
		r = hue2rgb(p, q, h + 1/3);
		g = hue2rgb(p, q, h);
		b = hue2rgb(p, q, h - 1/3);
	}

	return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
};


/**
 * Převod RGB formátu na HSV
 * @param r
 * @param g
 * @param b
 * @returns {*[]}
 */
jumbo.rgbToHSV = function(r, g, b) {
	var rr, gg, bb;
	r = r / 255;
	g = g / 255;
	b = b / 255;
	var h, s,
		v = Math.max(r, g, b),
		diff = v - Math.min(r, g, b),

		diffc = function(c){
			return (v - c) / 6 / diff + 1 / 2;
		};

	if (diff == 0) {
		h = s = 0;
	} else {
		s = diff / v;
		rr = diffc(r);
		gg = diffc(g);
		bb = diffc(b);

		if (r === v) {
			h = bb - gg;
		} else if (g === v) {
			h = (1 / 3) + rr - bb;
		} else if (b === v) {
			h = (2 / 3) + gg - rr;
		}

		if (h < 0) {
			h += 1;
		} else if (h > 1) {
			h -= 1;
		}
	}

	return [Math.round(h * 360), Math.round(s * 100), Math.round(v * 100)];
};


/**
 * Převod HSV formátu na RGB
 * @param h
 * @param s
 * @param v
 * @returns {*[]}
 */
jumbo.hsvToRGB = function(h, s, v) {
	h = h / 360;
	s = s / 100;
	v = v / 100;

	var r, g, b, i, f, p, q, t;

	i = Math.floor(h * 6);
	f = h * 6 - i;
	p = v * (1 - s);
	q = v * (1 - f * s);
	t = v * (1 - (1 - f) * s);

	switch (i % 6) {
		case 0: r = v, g = t, b = p; break;
		case 1: r = q, g = v, b = p; break;
		case 2: r = p, g = v, b = t; break;
		case 3: r = p, g = q, b = v; break;
		case 4: r = t, g = p, b = v; break;
		case 5: r = v, g = p, b = q; break;
	}

	return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
};
