/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.net                                 *
 ***************************************************
 * FileName: RangeSlider.js                  ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * Jezdec pro výběr číselné hodnoty nebo pro výběr rozsahu
 * @class
 */
jumbo.RangeSlider = Class({
	/**
	 * @constructor
	 * @param {Node|string} wrapEl Prvek do kterého bude slider vytvořen
	 * @param {number} width Šířka slideru
	 * @param {number} height Výška slideru
	 * @param {number} min Minimální hodnota na slideru
	 * @param {number} max Maximální hodnota na slideru
	 * @param {number} [cur] Výchozí pozice jezdce - default minimum
	 * @param {number} [secondCur] Pokud je hodnota zadána, stane se ze slideru výběr rozsahu - hodnota je pozice druhého/zadního jezdce
	 */
	constructor: function(wrapEl, width, height, min, max, cur, secondCur) {
		/**
		 * Dráha
		 * @type {Node}
		 */
		this.track = null;


		/**
		 * Hlavní jezdec
		 * @type {Node}
		 */
		this.rider = null;


		/**
		 * Druhý jezdec - pokud půjde o výběr rozsahu
		 * @type {Node}
		 */
		this.secondRider = null;


		/**
		 * Zaznamenává jestli se jedná o výběr rozsahu
		 * @type {boolean}
		 */
		this.isRangeSelect = !isNaN(secondCur);


		/**
		 * Zaznamenává, jestli se jedná o vertikální nebo horizontální slider
		 * @type {boolean}
		 */
		this.isVertical = (width < height);


		/**
		 * Callback volaný při změně hodnoty - až po puštění myši
		 */
		this.callback = function() {};


		/**
		 * Callback volaný při změně hodnoty i v průběhu tažení
		 */
		this.stepCallback = function() {};


		/**
		 * Minimální skok hodnoty
		 * @type {number}
		 */
		this.step = 0;


		/**
		 * Poslední hodnota jezdce
		 * @type {number}
		 */
		this.lastValue = cur || 0;


		/**
		 * Nová hodnota jezdce
		 * @type {number}
		 */
		this.newValue = cur || 0;


		/**
		 * Hodnota druhého jezdce
		 * @type {number}
		 */
		this.secondLastValue = secondCur || 0;


		/**
		 * Hodnota druhého jezdce
		 * @type {number}
		 */
		this.secondNewValue = secondCur || 0;


		/**
		 * Maximální hodnota
		 * @type {number}
		 */
		this.max = max;


		/**
		 * Minimální hodnota
		 * @type {number}
		 */
		this.min = min;


		/**
		 * Šířka slideru
		 * @type {number}
		 */
		this.width = width;


		/**
		 * Výška slideru
		 * @type {number}
		 */
		this.height = height;




		// Necháme vytvořit HTML část
		this.constructHTML(wrapEl, width, height, min, max, this.lastValue, this.secondLastValue);
	},


	/**
	 * Vytvoří HTML slideru
	 * @param wrapEl
	 * @param width
	 * @param height
	 * @param min
	 * @param max
	 * @param cur
	 * @param isRangeSelect
	 * @private
	 */
	constructHTML: function(wrapEl, width, height, min, max, cur, secondCur) {
		var self = this;

		if (max < min) {
			var tmp = max;
			max = min;
			min = tmp;
		}

		wrapEl = jumbo.get(wrapEl);

		var rozsah = max - min;
		var jednotka;



		// Obalový DIV
		var wrap = jumbo.create("div", {
			class: "jumbo-range-slider-wrap " + (this.isVertical ? "vertical" : "horizontal"),
			style: "width: " + width + "px;height: " + height + "px;"
		});



		// Dráha jezdce
		var trackStyle = "";

		if (this.isVertical) {
			trackStyle = "height: " + height + "px;";
		} else {
			trackStyle = "width: " + width + "px;";
		}

		this.track = wrap.appendChild(jumbo.create("div", {
			style: trackStyle,
			class: "jumbo-range-slider-track"
		}));



		var riderSize = null;
		var trackPos = jumbo.absPos(this.track);
		var startPos;
		var startCurPos;
		var mouseUpNow = false;


		var mouseMoveActionBase = function(isFirst, e) {
			e = e || window.event;

			var curPos = jumbo.curPos(e).relative;
			var mouseDist = self.isVertical ? -(startCurPos.y - curPos.y) : -(startCurPos.x - curPos.x);
			var newPos = startPos + mouseDist;
			var tmp;
			var newValue;

			if (newPos < 0) {
				newPos = 0;
			}

			if (self.isVertical) {
				if (newPos > height - riderSize.y) {
					newPos = height - riderSize.y;
				}

				if (self.step) {
					tmp = self.step * ((height - riderSize.y) / max);
					newPos = Math.round(newPos / tmp) * tmp;
				} else {
					newValue = newPos / ((height - riderSize.y) / max);
				}

				if (isFirst) {
					self.rider.style.top = newPos + "px";
				} else {
					if (this.isRangeSelect) self.secondRider.style.top = newPos + "px";
				}
			} else {
				if (newPos > width - riderSize.x) {
					newPos = width - riderSize.x;
				}

				if (self.step) {
					tmp = self.step * ((width - riderSize.x) / max);
					newPos = Math.round(newPos / tmp) * tmp;
				} else {
					newValue = newPos / ((width - riderSize.x) / max);
				}

				if (isFirst) {
					self.rider.style.left = newPos + "px";
				} else {
					if (this.isRangeSelect) self.secondRider.style.left = newPos + "px";
				}
			}


			if (self.step) {
				newValue = (newPos / tmp) * self.step;

				if (self.step >= 1) {
					newValue = Math.round(newValue);
				} else if (self.step < 1) {
					tmp = parseInt("1" + ((self.step).toString().match(/0/g) || []).join(""));
					newValue = Math.round(newValue * tmp) / tmp;
				}
			}

			if ( (isFirst && newValue != self.newValue) || (!isFirst && newValue != self.secondNewValue) ) {
				if (isFirst) {
					self.newValue = newValue;

					if (newValue > self.secondNewValue && self.isRangeSelect) {
						self.stepCallback(self.secondNewValue, newValue);
					} else {
						self.stepCallback(newValue, self.secondNewValue);
					}
				} else {
					self.secondNewValue = newValue;

					if (newValue > self.newValue) {
						self.stepCallback(self.newValue, newValue);
					} else {
						self.stepCallback(newValue, self.newValue);
					}
				}
			}
		};

		var mouseMoveAction = function(e) {
			mouseMoveActionBase(true, e);
		};

		var mouseMoveActionSecond = function(e) {
			mouseMoveActionBase(false, e);
		};



		this.rider = wrap.appendChild(jumbo.addEvent(jumbo.create("div", {
			class: "jumbo-range-slider-rider"
		}), "mousedown", function(e) {
			e = e || window.event;

//			if (riderSize == null) {
//				riderSize = {
//					x: parseInt(this.offsetWidth),
//					y: parseInt(this.offsetHeight)
//				};
//			}

			jumbo.disableSelection(document.body);

			startPos = (self.isVertical ? parseInt(this.style.top) : parseInt(this.style.left));
			startCurPos = jumbo.curPos(e).relative;

			jumbo.addEvent(window, "mousemove", mouseMoveAction);

			mouseUpNow = true;
		}));


		// Druhý jezdec v případě výběru rozsahu
		if (this.isRangeSelect) {
			this.secondRider = wrap.appendChild(jumbo.addEvent(jumbo.create("div", {
				class: "jumbo-range-slider-rider"
			}), "mousedown", function(e) {
				e = e || window.event;

//				if (riderSize == null) {
//					riderSize = {
//						x: parseInt(this.offsetWidth),
//						y: parseInt(this.offsetHeight)
//					};
//				}

				jumbo.disableSelection(document.body);

				startPos = (self.isVertical ? parseInt(this.style.top) : parseInt(this.style.left));
				startCurPos = jumbo.curPos(e).relative;

				jumbo.addEvent(window, "mousemove", mouseMoveActionSecond);

				mouseUpNow = true;
			}));
		}



		// Uvolnění tlačítka myši nad oknem
		jumbo.addEvent(window, "mouseup", function() {
			if (mouseUpNow) {
				jumbo.removeEvent(window, "mousemove", mouseMoveAction);
				jumbo.removeEvent(window, "mousemove", mouseMoveActionSecond);

				if (self.newValue != self.lastValue || self.secondNewValue != self.secondLastValue) {
					if (self.newValue != self.lastValue) {
						self.lastValue = self.newValue;
					} else {
						self.secondLastValue = self.secondNewValue;
					}

					if (self.newValue > self.secondNewValue && self.isRangeSelect) {
						self.callback(self.secondNewValue, self.newValue);
					} else {
						self.callback(self.newValue, self.secondNewValue);
					}
				}

				jumbo.enableSelection(document.body);

				mouseUpNow = false;
			}
		});


		wrapEl.appendChild(wrap);


		setTimeout(function() {
			riderSize = {
				x: parseInt(self.rider.offsetWidth) || 15,
				y: parseInt(self.rider.offsetHeight) || 15
			};

//			console.log(riderSize);

			jednotka = self.isVertical ? ((height - riderSize.y) / rozsah) : ((width - riderSize.x) / rozsah);

			if (self.isVertical) {
				self.rider.style.top = (jednotka * cur) + "px";
				if (self.isRangeSelect) self.secondRider.style.top = (jednotka * secondCur) + "px";
			} else {
				self.rider.style.left = (jednotka * cur) + "px";
				if (self.isRangeSelect) self.secondRider.style.left = (jednotka * secondCur) + "px";
			}
		}, 10);
	},


	/**
	 * Zapne skákání jezdce po hodnotách
	 * @param {number} step Základní jednotka
	 */
	jumpOverValues: function(step) {
		this.step = step;
	},


	/**
	 * Nastaví výstupní callback
	 * @param {Function} func
	 */
	setOutputCallback: function(func) {
		if (typeof func == "function") {
			this.callback = func;
		}
	},


	/**
	 * Nastaví výstupní callback
	 * @param {Function} func
	 */
	setStepCallback: function(func) {
		if (typeof func == "function") {
			this.stepCallback = func;
		}
	},


	/**
	 * Nastaví pozici prvního ukazatele
	 * @param {number} pos
	 */
	setPosition: function(pos) {
		return this.setRiderPos(pos, true);
	},


	/**
	 * Nastaví pozici druhého ukazatele
	 * @param {number} pos
	 */
	setSecondPosition: function(pos) {
		return this.setRiderPos(pos, false);
	},


	/**
	 * Privátní podprogram pro nastavení pozice jezdce
	 * @param pos
	 * @param isFirst
	 * @private
	 */
	setRiderPos: function(pos, isFirst) {
		if (isNaN(pos)) {
			if (jumbo.consoleLogging == true) console.log("Dosazená pozice není číslo.");
			return;
		}

		pos = parseInt(pos);

		if (pos < this.min) {
			pos = this.min;
		}

		if (pos > this.max) {
			pos = this.max;
		}

		var riderSize = {
			x: parseInt(this.rider.offsetWidth),
			y: parseInt(this.rider.offsetHeight)
		};
		var rozsah = this.max - this.min;
		var p = this.isVertical ? (((this.height - riderSize.y) / rozsah) * (pos || 0)) : (((this.width - riderSize.x) / rozsah) * (pos || 0));


		if (this.isVertical) {
			if (isFirst) {
				this.rider.style.top = p + "px";
			} else {
				if (this.isRangeSelect) this.secondRider.style.top = p + "px";
			}
		} else {
			if (isFirst) {
				this.rider.style.left = p + "px";
			} else {
				if (this.isRangeSelect) this.secondRider.style.left = p + "px";
			}
		}

		if (isFirst) {
			this.lastValue = this.newValue;
			this.newValue = pos;
		} else {
			this.secondLastValue = this.secondNewValue;
			this.secondNewValue = pos;
		}
	}
}).class;