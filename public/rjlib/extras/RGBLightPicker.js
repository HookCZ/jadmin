/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.net                                 *
 ***************************************************
 * FileName: RGBLightPicker.js                  ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

/**
 * @requires math.js, dinamics.js
 */

/**
 * Grafický výběr barvy
 * @param {Node} wrapEl Element do kterého bude RGBLightPicker vytvořen
 * @constructor
 */
jumbo.RGBLightPicker = function(wrapEl, callback) {
	/**
	 * Uložíme si obalový element
	 * @type {Node}
	 */
	this.wrap = jumbo.get(wrapEl);


	/**
	 * Zapíšeme callback fci
	 * @type {*}
	 */
	this.callback = callback || null;


	/**
	 * Výchozí barva
	 * @type {number[]}
	 */
	this.barva = [255, 0, 0];//[0, 0, 255];


	/**
	 * Aktuální barva
	 * @type {number[]}
	 */
	this.aktualniBarva = [0, 0, 255];


	/**
	 * Absolutní souřadnice wrap elementu
	 * @type {null}
	 */
	this.wrapPos = null;


	/**
	 * Jas ze slideru v %
	 * @type {number}
	 */
	this.jas = 100;


	if (!jumbo.isNode(this.wrap)) {
		if (jumbo.consoleLogging == true) console.log("Objekt dosazený pro vykreslování je chybný.");
		return;
	}


	// Sestavíme HTML část RGBLightPickeru
	this.sestavit();
};


/**
 * Sestaví HTML část RGBLightPickeru
 * @private
 */
jumbo.RGBLightPicker.prototype.sestavit = function() {
	var self = this;



	// Vytvoříme canvas na kruhový výběr
	this.colorPickCanvas = jumbo.create("canvas", {
		width:	220,
		height:	220,
		class:	"kruhovy-vyber"
	});

	// Vložíme obrázek kruhu
	var ctx = this.colorPickCanvas.getContext('2d');
	var img = new Image();
	img.src = "./images/color-pick-kruh.png";
	img.onload = function() {
		ctx.drawImage(img, 0, 0, img.width, img.height);
	};



	// Výběrový pointer
	this.colorPickPointer = jumbo.addEvent(jumbo.create("div", {
		class:	"barva-pointer"
	}), "mousedown", function(e) {
		e = e || window.event;
		startPos = jumbo.absPos(self.colorPickCanvas);

		var mouseMoveCallee = null;
		jumbo.addEvent(document, "mousemove", mouseMoveCallee = function(e) {
			e = e || window.event;

			if (mouseMoveCallee == null) {
				mouseMoveCallee = arguments.callee;
			}

			// Vypočteme nové souřadnice umístění pointru
			// Vytvoříme si vektor od středu k pozici myši
			var cur = jumbo.curPos(e);
			var stred = {
				x: /*self.colorPickCanvasPos*/startPos.x + 110,
				y: /*self.colorPickCanvasPos*/startPos.y + 110
			};
			var vCur = {
				x: cur.x - stred.x,
				y: cur.y - stred.y
			};

			// Vytvoříme vektor od středu doprava
			var vHor = {
				x: 110,
				y: 0
			};

			// Úhel svíraný vektory
			var uhel = jumbo.vectsAngle(vHor, vCur);

			// souřadnice pointru
			var p = {
				x: 97 * Math.cos(uhel * .017453292519943295),
				y: 97 * Math.sin(uhel * .017453292519943295)
			};

			p.x += 110;
			p.y += 110;

			// Umístíme pointer na pozici
			self.colorPickPointer.style.margin = (p.y - 218 - 13) + "px 0 0 " + (p.x - 11) + "px";

			self.zjistiBarvu(p.x, p.y);
		});

		jumbo.addEvent(document, "mouseup", function() {
			jumbo.removeEvent(document, "mousemove", mouseMoveCallee);
			jumbo.removeEvent(document, "mouseup", arguments.callee);
		});
	});

	// Event pro telefony
	try {
		jumbo.addEvent(this.colorPickPointer, "touchstart", function(e) {
			e = e || window.event;
			startPos = jumbo.absPos(self.colorPickCanvas);

			var mouseMoveCallee = null;
			jumbo.addEvent(document, "touchmove", mouseMoveCallee = function(e) {
				e = e || window.event;
				e.preventDefault();

				if (mouseMoveCallee == null) {
					mouseMoveCallee = arguments.callee;
				}

				if (e.touches.length != 1) {
					return;
				}

				var touch = e.touches[0];

				// Vypočteme nové souřadnice umístění pointru
				// Vytvoříme si vektor od středu k pozici myši
				var cur = {
					x:	touch.clientX,
					y:	touch.clientY
				};
				var stred = {
					x: /*self.colorPickCanvasPos*/startPos.x + 110,
					y: /*self.colorPickCanvasPos*/startPos.y + 110
				};
				var vCur = {
					x: cur.x - stred.x,
					y: cur.y - stred.y
				};

				// Vytvoříme vektor od středu doprava
				var vHor = {
					x: 110,
					y: 0
				};

				// Úhel svíraný vektory
				var uhel = jumbo.vectsAngle(vHor, vCur);

				// souřadnice pointru
				var p = {
					x: 97 * Math.cos(uhel * .017453292519943295),
					y: 97 * Math.sin(uhel * .017453292519943295)
				};

				p.x += 110;
				p.y += 110;

				// Umístíme pointer na pozici
				self.colorPickPointer.style.margin = (p.y - 218 - 13) + "px 0 0 " + (p.x - 11) + "px";

				self.zjistiBarvu(p.x, p.y);
			});

			jumbo.addEvent(document, "touchend", function() {
				jumbo.removeEvent(document, "touchmove", mouseMoveCallee);
				jumbo.removeEvent(document, "touchend", arguments.callee);
			});
		});
	} catch (ex) { }



	/**********************************************************************************
	 * Canvas pro výběr bílé
	 */
	this.whitePickCanvas = jumbo.create("canvas", {
		width:	115,
		height:	115,
		class:	"vyber-bile"
	});

	// Necháme sestavit obrázek pro výchozí barvu
	this.changeWhitePicker(this.barva[0], this.barva[1], this.barva[2]);

	// Nastavíme event na výběr
	jumbo.addEvent(this.whitePickCanvas, "mousedown", function(e) {
		// Zjistíme barvu už při kliku, pak až při pohybu
		e = e || window.event;
		self.zjistiBilou(e);

		var mouseMoveCallee = null;
		jumbo.addEvent(document, "mousemove", mouseMoveCallee = function(e) {
			if (mouseMoveCallee == null) {
				mouseMoveCallee = arguments.callee;
			}
			self.zjistiBilou(e);
		});

		jumbo.addEvent(document, "mouseup", function() {
			document.removeEvent("mousemove", mouseMoveCallee);
			document.removeEvent("mouseup", arguments.callee);
		});
	});



	/**********************************************************************************
	 * Slider pro výběr černé
	 */
	var blackSliderWrap = jumbo.create("div", {
		class: "slider"
	});

	this.blackSlider = new jumbo.RangeSlider(blackSliderWrap, 220, 30, 0, 100, 100);
	this.blackSlider.jumpOverValues(1);
	this.blackSlider.setStepCallback(function(val) {
		self.jas = val;
//		self.vybratBarvu(self.barva[0], self.barva[1], self.barva[2]);
		self.zmenaJasu();
	});

	var nastaveniJasu = jumbo.create("div", {
		class: "nastaveni-jasu",
		content: [
			jumbo.create("div", {
				class: "jas",
				content: "Jas"
			}),
			blackSliderWrap
		]
	});



	/**********************************************************************************
	 * RGBLightPicker vlastní wrap
	 */
	this.cpWrap = jumbo.create("div", {
		class:		"jumbo-color-picker-wrap",
		content:	[
			this.colorPickCanvas,
			this.whitePickCanvas,
			this.colorPickPointer,
			nastaveniJasu
		]
	});



	this.wrap.appendChild(this.cpWrap);
	this.setColorPickPointerPos();
};


/**
 * Nastavení barvy
 * @param r
 * @param g
 * @param b
 */
jumbo.RGBLightPicker.prototype.setBarva = function(r, g, b) {
	this.barva = [r, g, b];
	this.setColorPickPointerPos();
};


/**
 * Nastavení ukazatele na uloženou barvu this.barva
 * @private
 */
jumbo.RGBLightPicker.prototype.setColorPickPointerPos = function() {
	// Převedeme barvu na HSL a složku H přepočteme na úhel
	var hsl = jumbo.rgbToHSL(this.barva[0], this.barva[1], this.barva[2]);
	var uhel = hsl[0] * 2 * 3.14159 * 57.29577951308232; // H * 2 * PI -> *57.xxx -> převod na stupně z radiánů

	// souřadnice pointru
	var p = {
		x: 97 * Math.cos(uhel * .017453292519943295),
		y: 97 * Math.sin(uhel * .017453292519943295)
	};

	p.x += 110;
	p.y += 110;

	// Umístíme pointer na pozici
	this.colorPickPointer.style.margin = (p.y - 218 - 13) + "px 0 0 " + (p.x - 11) + "px";
};


/**
 * Zjistí barvu na kterou bylo v kruhu kliknuto
 * @param canvasX
 * @param canvasY
 * @private
 */
jumbo.RGBLightPicker.prototype.zjistiBarvu = function(canvasX, canvasY) {
	var imageData = this.colorPickCanvas.getContext('2d').getImageData(canvasX, canvasY, 1, 1);

	// Najeli jsme myší mimo a vybrala se tam černá,.. to nechceme
	if (imageData.data[0] == 0 && imageData.data[1] == 0 && imageData.data[2] == 0) {
		return;
	}

	this.vybratBarvu(imageData.data[0], imageData.data[1], imageData.data[2]);

	// Barvu máme, teď upravíme výběr bílé
	this.changeWhitePicker(imageData.data[0], imageData.data[1], imageData.data[2]);
};


/**
 * Zjistí prvek bílé u vybrané barvy
 * @param e Event
 * @private
 */
jumbo.RGBLightPicker.prototype.zjistiBilou = function(e) {
	e = e || window.event;

	var t = jumbo.eventTarget(e);

	if (t != this.whitePickCanvas) {
		return;
	}

	var cur = jumbo.curPos(e);
	var pos = jumbo.absPos(this.whitePickCanvas);//.absPos();
	var canvasX = Math.floor(cur.x - pos.x);
	var canvasY = Math.floor(cur.y - pos.y);
	var imageData = this.whitePickCanvas.getContext('2d').getImageData(canvasX, canvasY, 1, 1);

	// Najeli jsme myší mimo a vybrala se tam černá,.. to nechceme
	if (imageData.data[0] == 0 && imageData.data[1] == 0 && imageData.data[2] == 0) {
		return;
	}

	this.vybratBarvu(imageData.data[0], imageData.data[1], imageData.data[2]);
};


/**
 * Překreslí pruh s výběrem bílé
 * @param r Červená decimálně
 * @param g Zelená
 * @param b Modrá
 * @private
 */
jumbo.RGBLightPicker.prototype.changeWhitePicker = function(r, g, b) {
	var ctx = this.whitePickCanvas.getContext('2d');

	var grd = ctx.createLinearGradient(5,0,110,0);
	grd.addColorStop(0, "rgb(" + r + ", " + g + ", " + b + ")");
	grd.addColorStop(1, "#fff");

	ctx.fillStyle = grd;
	ctx.fillRect(0,0,115,115);
};


/**
 * Zapíšeme si danou barvu a zobrazíme ji v náhledovém boxu
 * @param r
 * @param g
 * @param b
 * @private
 */
jumbo.RGBLightPicker.prototype.vybratBarvu = function(r, g, b) {
	var hsv = jumbo.rgbToHSV(r, g, b);
	var rgb = jumbo.hsvToRGB(hsv[0], hsv[1], this.jas)

	r = rgb[0];
	g = rgb[1];
	b = rgb[2];

	this.barva = [r, g, b];
	this.aktualniBarva = [this.barva[0], this.barva[1], this.barva[2]];

	if (this.callback != null) {
		this.callback(this.barva[0], this.barva[1], this.barva[2]);
	}
};


/**
 * Přepočítání barvy se zvoleným jasem
 * @private
 */
jumbo.RGBLightPicker.prototype.zmenaJasu = function() {
	var rgb = this.barva;
	var hsv = jumbo.rgbToHSV(rgb[0], rgb[1], rgb[2]);
	rgb = jumbo.hsvToRGB(hsv[0], hsv[1], this.jas)

	if (this.callback != null) {
		this.callback(rgb[0], rgb[1], rgb[2]);
	}
};
