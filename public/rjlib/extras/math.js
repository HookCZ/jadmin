/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.net                                 *
 ***************************************************
 * FileName: math.js                         ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/


/**
 * Převede stupně na radiány
 * @param d number
 * @returns number
 */
jumbo.deg2rad = function(d) {
	return (d / 180) * Math.PI
};

/**
 * Převede radiány na stupně
 * @param r number
 * @returns number
 */
jumbo.rad2deg = function(r) {
	return (r / Math.PI) * 180;
};

/**
 * Vrátí velikost vektory svírajícího úhlu
 * @param u array - vektor
 * @param v array - vektor
 * @returns number - úhel ve stupních
 */
jumbo.vectsAngle = function(u, v) {
	var atanA = Math.atan2(u.x, u.y);
	var atanB = Math.atan2(v.x, v.y);

	return jumbo.rad2deg(atanA - atanB);
};