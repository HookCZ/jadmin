
SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for categories
-- ----------------------------
CREATE TABLE `categories` (
  `category_id` mediumint(8) unsigned NOT NULL auto_increment,
  `menu_id` mediumint(8) unsigned NOT NULL,
  `parent` mediumint(8) unsigned default NULL,
  `name` varchar(50) NOT NULL,
  `visible` tinyint(4) NOT NULL default '0',
  `hlavni` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`category_id`),
  KEY `fk_categories_menu_id__menus` (`menu_id`),
  KEY `fk_categories_parent_categories` (`parent`),
  CONSTRAINT `fk_categories_menu_id__menus` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_categories_parent_categories` FOREIGN KEY (`parent`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for menus
-- ----------------------------
CREATE TABLE `menus` (
  `menu_id` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `position` varchar(10) NOT NULL,
  `visible` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for page_categories
-- ----------------------------
CREATE TABLE `page_categories` (
  `page_id` int(10) unsigned NOT NULL,
  `category_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY  (`page_id`,`category_id`),
  KEY `fk_pages_categories_category_categories` (`category_id`),
  CONSTRAINT `fk_pages_categories_category_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_categories_page_pages` FOREIGN KEY (`page_id`) REFERENCES `pages` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pages
-- ----------------------------
CREATE TABLE `pages` (
  `page_id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `text` text NOT NULL,
  `clanky` tinyint(4) NOT NULL default '0',
  `visible` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_catalog_categories
-- ----------------------------
CREATE TABLE `shop_catalog_categories` (
  `catalog_id` mediumint(8) unsigned NOT NULL,
  `category_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY  (`catalog_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_catalogs
-- ----------------------------
CREATE TABLE `shop_catalogs` (
  `catalog_id` smallint(5) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY  (`catalog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_dph
-- ----------------------------
CREATE TABLE `shop_dph` (
  `dph_id` mediumint(8) unsigned NOT NULL auto_increment,
  `value` decimal(5,2) NOT NULL,
  PRIMARY KEY  (`dph_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_products
-- ----------------------------
CREATE TABLE `shop_products` (
  `product_id` int(10) unsigned NOT NULL auto_increment,
  `kod` varchar(20) default NULL,
  `name` varchar(100) NOT NULL,
  `popis` text NOT NULL,
  `cena` decimal(10,2) NOT NULL default '0.00',
  `dph_id` mediumint(8) unsigned NOT NULL,
  `zaruka_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY  (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_zaruka
-- ----------------------------
CREATE TABLE `shop_zaruka` (
  `zaruka_id` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY  (`zaruka_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
