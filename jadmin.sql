/*
MySQL Data Transfer
Source Host: localhost
Source Database: jadmin
Target Host: localhost
Target Database: jadmin
Date: 03.02.2016 23:13:38
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for categories
-- ----------------------------
CREATE TABLE `categories` (
  `category_id` mediumint(8) unsigned NOT NULL auto_increment,
  `menu_id` mediumint(8) unsigned NOT NULL,
  `parent` mediumint(8) unsigned default NULL,
  `name` varchar(50) NOT NULL,
  `visible` tinyint(3) unsigned NOT NULL default '0',
  `hlavni` tinyint(3) unsigned NOT NULL default '0',
  `order` varchar(2) NOT NULL default 'id',
  PRIMARY KEY  (`category_id`),
  KEY `fk_categories_menu_id__menus` (`menu_id`),
  KEY `fk_categories_parent_categories` (`parent`),
  CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`menu_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `categories_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clanky
-- ----------------------------
CREATE TABLE `clanky` (
  `clanek_id` int(10) unsigned NOT NULL auto_increment,
  `page_id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `date` bigint(20) unsigned NOT NULL,
  `visible` tinyint(3) unsigned NOT NULL default '0',
  `seo_url` varchar(60) NOT NULL default 'error',
  PRIMARY KEY  (`clanek_id`),
  KEY `fk_clanky_page_id__pages_page_id` (`page_id`),
  CONSTRAINT `fk_clanky_page_id__pages_page_id` FOREIGN KEY (`page_id`) REFERENCES `pages` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for menus
-- ----------------------------
CREATE TABLE `menus` (
  `menu_id` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `position` varchar(10) NOT NULL,
  `visible` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for page_categories
-- ----------------------------
CREATE TABLE `page_categories` (
  `page_id` int(10) unsigned NOT NULL,
  `category_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY  (`page_id`,`category_id`),
  KEY `fk_pages_categories_category_categories` (`category_id`),
  CONSTRAINT `page_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `page_categories_ibfk_2` FOREIGN KEY (`page_id`) REFERENCES `pages` (`page_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pages
-- ----------------------------
CREATE TABLE `pages` (
  `page_id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `text` text NOT NULL,
  `clanky` tinyint(3) unsigned NOT NULL default '0',
  `visible` tinyint(3) unsigned NOT NULL default '0',
  `seo_url` varchar(60) NOT NULL default 'error',
  `is_home` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_catalog_categories
-- ----------------------------
CREATE TABLE `shop_catalog_categories` (
  `catalog_id` mediumint(8) unsigned NOT NULL,
  `category_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY  (`catalog_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_catalogs
-- ----------------------------
CREATE TABLE `shop_catalogs` (
  `catalog_id` smallint(5) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY  (`catalog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_dph
-- ----------------------------
CREATE TABLE `shop_dph` (
  `dph_id` mediumint(8) unsigned NOT NULL auto_increment,
  `value` decimal(5,2) NOT NULL,
  PRIMARY KEY  (`dph_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_products
-- ----------------------------
CREATE TABLE `shop_products` (
  `product_id` int(10) unsigned NOT NULL auto_increment,
  `kod` varchar(20) default NULL,
  `name` varchar(100) NOT NULL,
  `popis` text NOT NULL,
  `cena` decimal(10,2) NOT NULL default '0.00',
  `dph_id` mediumint(8) unsigned NOT NULL,
  `zaruka_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY  (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_zaruka
-- ----------------------------
CREATE TABLE `shop_zaruka` (
  `zaruka_id` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY  (`zaruka_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for users
-- ----------------------------
CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL auto_increment,
  `email` varchar(80) NOT NULL,
  `pass_hash` varchar(40) NOT NULL,
  `prezdivka` varchar(30) default NULL,
  `jmeno` varchar(30) default NULL,
  `prijmeni` varchar(30) default NULL,
  `ulice` varchar(80) default NULL,
  `mesto` varchar(80) default NULL,
  `psc` varchar(8) default NULL,
  `ico` varchar(15) default NULL,
  `dic` varchar(15) default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

